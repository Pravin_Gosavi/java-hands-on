/**
 * Created by pravingosavi on 14/06/17.
 */
abstract class AbstractClassDemo {

    abstract void sum();

}

//concrete class - because it is implementing the abstract method from its base class
class IntegerSum extends AbstractClassDemo{

    void sum(){
        int a,b,c;
        a=10;
        b=20;
        c=a+b;
        System.out.println("Sum of a snd b ="+c);
    }
}

//concrete class - because it is implementing the abstract method from its base class
class FloatSum extends AbstractClassDemo{
    void sum(){
        float f1,f2,f3;
        f1=10;
        f2=30;
        f3=f1+f2;
        System.out.println("Sum of f1 and f2 ="+f3);
    }
}

//abstract derived class - it is not implementing any abstract method from its base class
abstract class DoubleSum extends AbstractClassDemo{

}