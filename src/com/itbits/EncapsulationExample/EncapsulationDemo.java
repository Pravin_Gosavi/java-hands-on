package com.itbits.EncapsulationExample;

/**
 * Created by pravingosavi on 20/06/17.
 */
public class EncapsulationDemo {

    //Encapsulation in java is a process of wrapping code and data together into a single unit, for example capsule i.e. mixed of several medicines.
    //We can create a fully encapsulated class in java by making all the data members of the class private.
    //Now we can use setter and getter methods to set and get the data in it.
    //The Java Bean class is the example of fully encapsulated class.
    //By providing only setter or getter method, you can make the class read-only or write-only.
    //It provides you the control over the data. Suppose you want to set the value of id i.e. greater than 100 only, you can write the logic inside the setter method.


    private String name;
    private String address;
    private String contact;

    public EncapsulationDemo(String name, String address, String contact) {
        this.name = name;
        this.address = address;
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
