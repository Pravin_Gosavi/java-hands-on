package com.itbits.ConstuctorDemo;

/**
 * Created by pravingosavi on 13/06/17.
 */
public class SuperAtDerivedClass extends SuperAtIntermediateBaseClass {

    public SuperAtDerivedClass(){
        super(10);
        System.out.println("Derived class - default constructor");
    }

    SuperAtDerivedClass(int a){
        this();
        System.out.println("Derived class - parameterized constructor");
    }
}
