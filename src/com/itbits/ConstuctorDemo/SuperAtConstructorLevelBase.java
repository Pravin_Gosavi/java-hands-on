package com.itbits.ConstuctorDemo;

/**
 * Created by pravingosavi on 13/06/17.
 */
public class SuperAtConstructorLevelBase {

    SuperAtConstructorLevelBase(){
        System.out.println("Base class - default constructor");
    }

    SuperAtConstructorLevelBase(int a){
        this();
        System.out.println("Base class - parameterized constructor");
    }

}
