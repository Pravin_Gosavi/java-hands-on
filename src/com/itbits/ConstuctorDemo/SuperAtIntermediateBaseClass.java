package com.itbits.ConstuctorDemo;

/**
 * Created by pravingosavi on 13/06/17.
 */
public class SuperAtIntermediateBaseClass extends SuperAtConstructorLevelBase {

    SuperAtIntermediateBaseClass(){
        super(100);
        System.out.println("Intermediate Base class - default constructor");
    }

    SuperAtIntermediateBaseClass(int x){
        this();
        System.out.println("Intermediate Base class - parameterized constructor");

    }
}
