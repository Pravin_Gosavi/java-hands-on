package com.itbits.inheritancedemo2;

import com.itbits.inheritancedemo1.BaseClass1;

/**
 * Created by pravingosavi on 14/06/17.
 */

//Is-A Relationship and across the package
public class BaseClass2 extends BaseClass1{


    public void display(){
        //In Is-A relationship Data members declared in a class from different package is not accessible here, only public and protected data members are accessible from Base Class

        //System.out.println("Private Variable from Base Class = "privateVariable);
        //System.out.println("Default Variable from Base Class = "defaultVariable);
        System.out.println("Protected Variable from Base Class = "+protectedVariable);
        System.out.println("Public Variable from Base Class = "+publicVariable);
    }
}
