package com.itbits.Multithreading;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * Created by pravingosavi on 20/09/18.
 */
public class Consumer implements Runnable {

    private BlockingQueue<Message> queue;

    public Consumer(BlockingQueue<Message> queue){
        this.queue = queue;
    }

    @Override
    public void run() {
        try{
            Message message;
            while ((message = queue.take()).getMessage() != "Exit"){
                Thread.sleep(10);
                System.out.println("Consumed..."+message.getMessage());
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
