package com.itbits.Multithreading;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadFactory;

/**
 * Created by pravingosavi on 20/09/18.
 */
public class Worker extends Thread {

    private int delay;
    private CountDownLatch countDownLatch;

    public Worker(CountDownLatch countDownLatch, int delay,String name){
        super(name);
        this.countDownLatch = countDownLatch;
        this.delay = delay;
    }


    public void run(){
        try{
            Thread.sleep(delay);
            countDownLatch.countDown();
            System.out.println(Thread.currentThread().getName() + " Finished");
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
