package com.itbits.Multithreading;

/**
 * Created by pravingosavi on 20/09/18.
 */
public class Message {

    private  String message;

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
