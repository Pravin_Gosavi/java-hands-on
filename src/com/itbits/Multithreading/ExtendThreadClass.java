package com.itbits.Multithreading;

/**
 * Created by pravingosavi on 19/09/18.
 */
public class ExtendThreadClass extends Thread {

    public void run(){
        try{
            System.out.println("Thread " + Thread.currentThread().getId() + " is running...");
        }catch(Exception e){
            System.out.println("Exception Caught...");
        }
    }
}
