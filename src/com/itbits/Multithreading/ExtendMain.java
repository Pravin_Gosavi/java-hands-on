package com.itbits.Multithreading;

/**
 * Created by pravingosavi on 19/09/18.
 */
public class ExtendMain {

    public static void main(String[] args) {
        int n = 8;
        System.out.println(" === By Extending Thread Class");
        for(int i =0; i<n; i++){
            ExtendThreadClass extendThreadClass = new ExtendThreadClass();
            extendThreadClass.start();
        }

        System.out.println(" === By Implementing Runnable Class");
        for(int j=0; j<n;j++){
            Thread thread = new Thread(new ImplementRunnableInterface());
            thread.start();
        }
    }
}
