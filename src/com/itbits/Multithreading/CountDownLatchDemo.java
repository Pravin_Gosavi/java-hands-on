package com.itbits.Multithreading;

import java.util.concurrent.CountDownLatch;

/**
 * Created by pravingosavi on 20/09/18.
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(4);

        Worker first = new Worker(countDownLatch, 1000, "Worker-1");
        Worker second = new Worker(countDownLatch, 2000, "Worker-2");
        Worker third = new Worker(countDownLatch, 3000, "Worker-3");
        Worker fourth = new Worker(countDownLatch, 4000, "Worker-4");

        first.start();
        second.start();
        third.start();
        fourth.start();

        countDownLatch.await();

        System.out.println(Thread.currentThread().getName() +" Has Finished..");


    }
}
