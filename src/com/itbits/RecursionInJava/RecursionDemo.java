package com.itbits.RecursionInJava;

/**
 * Created by pravingosavi on 20/06/17.
 */
public class RecursionDemo {

    //Recursion in java is a process in which a method calls itself continuously.
    //A method in java that calls itself is called recursive method.

    int count=0, n1=0,n2=1,n3=0;


    public void recursionExample(){
        count++;
        if(count <=5){
            System.out.println("Recursion");
            recursionExample();
        }
    }

    public int factorialCalulation(int n){
        if(n==1){
            return 1;
        }else{
            return (n * factorialCalulation(n-1));
        }
    }

    public void factorialUsingRecursion(){
        System.out.println("Factorial of 5 : "+factorialCalulation(5));
    }

    public void printFabonacci(int numCount){
        if(numCount>0){
            n3=n1+n2;
            n1=n2;
            n2=n3;
            System.out.print(" "+n3);
            printFabonacci(numCount-1);
        }
    }

    public void fabonacciSeriesUsingRecursion(){
        System.out.print(n1+" "+n2);
        printFabonacci(15);
    }
}
