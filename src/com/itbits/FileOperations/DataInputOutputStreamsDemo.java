package com.itbits.FileOperations;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class DataInputOutputStreamsDemo {

    //DataInputStream - This is used for two purposes. They are reading the data from input device like keyboard and reading the data from remote machine like server.
    //Ex. DataInputStream dis = new DataInputStream(System.in);

    //DataOutputStream - This is used for displaying the data onto the console and also used for writing the data to remote server
    //Ex. DataOutputStream dos = new DataOutputStream(System.out);

    public void calculateProduct(){
        int a,b,c;
        DataInputStream dis=new DataInputStream(System.in);

        try {
            System.out.println("Enter value for a = ");
            a = Integer.parseInt(dis.readLine());
            System.out.println("Enter value for b = ");
            b = Integer.parseInt(dis.readLine());
            c = a*b;
            System.out.println("Product of a and b = "+c);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }






}
