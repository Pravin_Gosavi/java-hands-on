package com.itbits.FileOperations;

import java.io.*;
import java.util.Properties;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class FileOperationsDemo {

    private String fileName="/Users/pravingosavi/Desktop/3.txt";

    public void writeToFile(){
        try{
            FileOutputStream fileOutputStream=new FileOutputStream(fileName,true);
            for(int i=0;i<=100;i++){
                fileOutputStream.write(i);
                System.out.println("Success");
            }

            fileOutputStream.close();
        }catch(IOException ioe){
            System.out.println(ioe);
        }catch(ArrayIndexOutOfBoundsException aiobe){
            System.out.println(aiobe);
        }
    }

    public void readFromFile(){
        try (FileInputStream fileInputStream = new FileInputStream(fileName)) {
            int i;
            while((i=fileInputStream.read())!=-1){
                System.out.println(i);
            }
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showFileListFromFolder(){
        File file = new File("/Users/pravingosavi/Desktop");
        String[] fileList=file.list();
        for(String name : fileList){
            System.out.println(name);
        }
    }

    public void showFilesByExtension(){
        File file = new File("/Users/pravingosavi/Desktop");
        String[] fileList=file.list(new FilenameFilter() {

            @Override
            public boolean accept(File pathname,String name) {
                if(name.toLowerCase().endsWith(".txt")){
                    return true;
                }else{
                    return false;
                }
            }
        });

        for(String name : fileList){
            System.out.println(name);
        }
    }

    public void readFileContentsAsByteArray(){
        InputStream inputStream=null;
        try{
            inputStream=new FileInputStream(fileName);
            byte content[]=new byte[2*1024];
            int readCount = 0;
            while ((readCount=inputStream.read(content))>0) {
                System.out.println(new String(content,0,readCount-1));
            }
        }catch(FileNotFoundException fnfe){
            System.out.println(fnfe);
        }catch(IOException ioe){
            System.out.println(ioe);
        }finally {
            if(inputStream!=null) try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void setFilePermissions(){
        File file=new File("/Users/pravingosavi/Desktop/ec2.pem");
        System.out.println("FIle Permissions : ");
        System.out.println("Can Execute : "+file.canExecute());
        System.out.println("Can Read : "+file.canRead());
        System.out.println("Can Write : "+file.canWrite());
        file.setExecutable(true);
        file.setReadable(true);
        file.setWritable(true);
        System.out.println("Can Execute : "+file.canExecute());
        System.out.println("Can Read : "+file.canRead());
        System.out.println("Can Write : "+file.canWrite());

    }






}
