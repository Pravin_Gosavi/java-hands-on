package com.itbits.FileOperations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class DataOutputStreamWriteFile {

    DataOutputStream dataOutputStream;
    private String fileName="/Users/pravingosavi/Desktop/student.txt";

    public void write(Student student) throws IOException {
        dataOutputStream.writeUTF(student.getName());
        dataOutputStream.writeUTF(student.getGender());
        dataOutputStream.writeInt(student.getAge());
        dataOutputStream.writeFloat(student.getGrade());
    }

    public void save(){
        try {
            dataOutputStream=new DataOutputStream(new FileOutputStream(fileName));
            List<Student> studentList=new ArrayList<>();
            studentList.add(new Student("Pravin","Male",30,90.0F));
            studentList.add(new Student("Payal","Female",24,80.0F));
            studentList.add(new Student("Sandip","Male",32,70.0F));
            studentList.add(new Student("Sachin","Male",33,95.0F));
            for(Student student : studentList){
                this.write(student);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException ioe){
            System.out.println(ioe);
        }
        finally{
            if(dataOutputStream!=null){
                try {
                    dataOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }





}
