package com.itbits.FileOperations;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class DataInputStreamReadFile {

    DataInputStream dataInputStream;
    private String fileName="/Users/pravingosavi/Desktop/student.txt";

    public List<Student> getAllStudents(){
        List<Student> studentArrayList=new ArrayList<>();
        try {
            dataInputStream=new DataInputStream(new FileInputStream(fileName));
            while(dataInputStream.available()>0){
                String name=dataInputStream.readUTF();
                String gender=dataInputStream.readUTF();
                int age=dataInputStream.readInt();
                float grade=dataInputStream.readFloat();

                Student student=new Student(name,gender,age,grade);
                studentArrayList.add(student);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(dataInputStream!=null){
                try {
                    dataInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return studentArrayList;

    }

    public void readStudentData(){
        List<Student> studentList=getAllStudents();
        for(Student student : studentList){
            System.out.print("Student Name : "+student.getName()+"\t");
            System.out.print("Student Gender : "+student.getGender()+"\t");
            System.out.print("Student Age : "+student.getAge()+"\t");
            System.out.print("Student Grade : "+student.getGrade()+"\t");
        }
    }
}
