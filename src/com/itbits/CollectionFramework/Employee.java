package com.itbits.CollectionFramework;

/**
 * Created by pravingosavi on 14/09/18.
 */
public class Employee {

    private String employeeId;
    private int salary;

    public Employee(String employeeId, int salary) {
        this.employeeId = employeeId;
        this.salary = salary;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId='" + employeeId + '\'' +
                ", salary=" + salary +
                '}';
    }
}
