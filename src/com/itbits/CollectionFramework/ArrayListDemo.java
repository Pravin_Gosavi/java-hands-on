package com.itbits.CollectionFramework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by pravingosavi on 14/09/18.
 */
public class ArrayListDemo {

    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("A");
        arrayList.add("B");
        arrayList.add("C");
        arrayList.add("D");
        arrayList.add("E");
        arrayList.add("F");
        arrayList.add("G");

        System.out.println("Origional List : "+arrayList.toString());

        System.out.println("=========================================================");
        System.out.println("Swap two elements in ArrayList");

        Collections.swap(arrayList,2,5);
        System.out.println(arrayList);

        System.out.println("=========================================================");
        System.out.println("Read All elements in ArrayList Using Itereator");

        Iterator<String> stringIterator = arrayList.iterator();
        while(stringIterator.hasNext()){
            System.out.println(stringIterator.next());
        }

        System.out.println("=========================================================");
        System.out.println("Cpoy OR Clone ArrayList");

        ArrayList<String> stringArrayList = (ArrayList<String>) arrayList.clone();
        System.out.println(stringArrayList);


        System.out.println("=========================================================");
        System.out.println("Cpoy All elements of List into ArrayList");

        List<String> list = new ArrayList<String>();
        list.add("H");
        list.add("I");
        arrayList.addAll(list);
        System.out.println("ArrayList after copied List elements"+arrayList.toString());


        System.out.println("=========================================================");
        System.out.println("Sort ArrayList using Comparator");
        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(new Employee("A", 30000));
        employeeList.add(new Employee("B", 20000));
        employeeList.add(new Employee("C", 10000));
        employeeList.add(new Employee("D", 40000));
        employeeList.add(new Employee("E", 50000));
        Collections.sort(employeeList , new ArrayListComparator());
        System.out.println("Sorted List : " + employeeList.toString());

    }
}
