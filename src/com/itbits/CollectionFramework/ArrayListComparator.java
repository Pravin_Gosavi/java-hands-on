package com.itbits.CollectionFramework;

import java.util.Comparator;

/**
 * Created by pravingosavi on 14/09/18.
 */
public class ArrayListComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        if(o1.getSalary() < o2.getSalary()){
            return 1;
        }else{
            return -1;
        }
    }
}
