package com.itbits.DataAbstractionDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public abstract class AbstractClassWithConstructor {

    //An abstract class can have data member, abstract method, method body, constructor and even main() method.

    AbstractClassWithConstructor(){
        System.out.println("Created...");
    }

    public abstract void run();

    public void destroy(){
        System.out.println("Destroyed...");
    }

}
