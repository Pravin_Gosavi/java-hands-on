package com.itbits.DataAbstractionDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public abstract class Shape {

    public abstract void draw();
}
