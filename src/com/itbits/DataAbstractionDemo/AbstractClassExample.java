package com.itbits.DataAbstractionDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
abstract class AbstractClassExample {

    //A class that is declared with abstract keyword, is known as abstract class in java.
    //It can have abstract and non-abstract methods (method with body).

    //Abstraction is a process of hiding the implementation details and showing only functionality to the user.

    //Abstraction lets you focus on what the object does instead of how it does it.

    /**
     Another way, it shows only important things to the user and hides the internal details for example sending sms,
     you just type the text and send the message. You don't know the internal processing about the message delivery.
     */

    //Ways to Achieve Abstraction in Java
    //1. Abstract Class
    //2. Interface

    /**
     A class that is declared as abstract is known as abstract class.
     It needs to be extended and its method implemented. It cannot be instantiated.

     A method that is declared as abstract and does not have implementation is known as abstract method.
     */


        abstract void run();






}

