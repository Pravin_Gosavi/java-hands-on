package com.itbits.ExceptionHandling;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class ThrowsBlockDemo {

    //Throws - - This is the keyword which gives an indication to the calling function to keep the called function under try and catch blocks.
    public void division() throws ArithmeticException,NumberFormatException{
        int a=1,b=0,c;
        c=a/b;
        System.out.println("Division of a and b = "+c);
    }

}


