package com.itbits.ExceptionHandling;

public class TryCatchBlock{

    public void callDivisionMethod(){
        try{
            new ThrowsBlockDemo().division();
        }catch(ArithmeticException ae){
            System.out.println("You cannot provide 0 as denominator");
        }catch(NumberFormatException nfe){
            System.out.println("Please provide Integers only");
        }
    }
}
