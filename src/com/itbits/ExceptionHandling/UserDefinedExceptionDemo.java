package com.itbits.ExceptionHandling;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class UserDefinedExceptionDemo {

    public void checkNumber1() throws InvalidNumberException{
        int a= -10;
        if(a<=0){
            InvalidNumberException invalidNumberException=new InvalidNumberException("Number is greater than 10");
            throw(invalidNumberException);
        }
    }

    public void checkNumber2(){
        try{
           checkNumber1();
        }catch(InvalidNumberException ine){
            System.out.println(ine);
        }
    }
}
