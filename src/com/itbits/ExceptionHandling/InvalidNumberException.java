package com.itbits.ExceptionHandling;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class InvalidNumberException extends Exception {

    InvalidNumberException(String s){
        super(s);
    }
}
