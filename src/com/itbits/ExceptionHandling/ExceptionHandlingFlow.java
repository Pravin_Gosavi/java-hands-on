package com.itbits.ExceptionHandling;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class ExceptionHandlingFlow {

    //1. Whenever we pass irrelevant input to a JAVA program, JVM cannot process the irrelevant input.

    //2. Since JVM is unable to process by user input, hence it can contact to JRE for getting an appropriate exception class.

    //3. JRE contacts to java.lang.Throwable for finding what type of exception it is.

    //4. java.lang.Throwable decides what type of exception it is and pass the message to JRE.

    //5. JRE pass the type of exception to JAVA API.

    //6. From the JAVA API either java.lang.Error class or java.lang.Exception class will found an appropriate sub class exception.

    //7. Either java.lang.Error class or java.lang.Exception class gives an appropriate exception class to JRE.

    //8. JRE will give an appropriate exception class to JVM.

    //9. JVM will create an object of appropriate exception class which is obtained from JRE and it generates system error message.

    //10. In order to make the program very strong (robust), JAVA programmer must convert the system error messages into user friendly messages by using the concept of exceptional handling.

    // User friendly messages are understand by normal user effectively hence our program is robust.

}
