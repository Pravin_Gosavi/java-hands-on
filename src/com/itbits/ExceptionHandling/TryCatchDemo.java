package com.itbits.ExceptionHandling;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class TryCatchDemo {

    public void deplayExceptions(){
        try{
            int a=1,b=0,c;
            c = a/b;
        }catch(ArithmeticException ae){
            System.out.println("You cannot provide 0 as denominator");
        }catch(NumberFormatException nfe){
            System.out.println("Please provide Integres only");
        }finally{
            System.out.println("I am in Finally block");
        }
    }

}
