package com.itbits.ExceptionHandling;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class FindDetailsOfException {

    public void usingAnObjectOfJavaException(){
        try{
            int a=1,b=0,c;
            c=a/b;
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public void usingPrintStackTrace(){
        try{
            int a=1,b=0,c;
            c=a/b;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void usingGetMessage(){
        try{
            int a=1,b=0,c;
            c=a/b;
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
