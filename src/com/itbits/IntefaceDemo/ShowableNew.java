package com.itbits.IntefaceDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public interface ShowableNew extends PrintableNew{
    public void show();
}
