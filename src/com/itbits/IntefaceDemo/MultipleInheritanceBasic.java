package com.itbits.IntefaceDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public class MultipleInheritanceBasic implements Printable,Showable{

    /**
     If a class implements multiple interfaces, or an interface extends multiple interfaces i.e. known as multiple inheritance.
     */

    @Override
    public void show() {
        System.out.println("Showed...");
    }

    @Override
    public void print() {
        System.out.println("Printed...");
    }

}
