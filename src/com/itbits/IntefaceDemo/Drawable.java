package com.itbits.IntefaceDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public interface Drawable {
    abstract void draw();
}
