package com.itbits.IntefaceDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public class InterfaceBasic {

    /**
     An interface in java is a blueprint of a class.

     It has static constants and abstract methods.

     The interface in java is a mechanism to achieve abstraction.

     There can be only abstract methods in the java interface not method body.

     It is used to achieve abstraction and multiple inheritance in Java.

     Java Interface also represents IS-A relationship.

     It cannot be instantiated just like abstract class.
     */


    /**
     Why use Java interface?

     There are mainly three reasons to use interface. They are given below.

     1. It is used to achieve abstraction.
     2. By interface, we can support the functionality of multiple inheritance.
     3. It can be used to achieve loose coupling.

     */
}
