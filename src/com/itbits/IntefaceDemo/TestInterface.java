package com.itbits.IntefaceDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */
public class TestInterface implements ShowableNew{
    @Override
    public void print() {
        System.out.println("Printing Done...");
    }

    @Override
    public void show() {
        System.out.println("Showing Done...");
    }
}
