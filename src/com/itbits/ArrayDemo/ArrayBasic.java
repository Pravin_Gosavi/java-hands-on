package com.itbits.ArrayDemo;

/**
 * Created by pravingosavi on 19/06/17.
 */
public class ArrayBasic {

    //array is a collection of similar type of elements that have contiguous memory location.
    //Java array is an object the contains elements of similar data type. It is a data structure where we store similar elements.
    //We can store only fixed set of elements in a java array.
    //Array in java is index based, first element of the array is stored at 0 index.

    //Advantages of Array
    //1. Code Optimization - It makes the code optimized, we can retrieve or sort the data easily.
    //2. Random Access - We can get any data located at any index position.

    //Disadvantages of Array
    //Size Limit: We can store only fixed size of elements in the array.
    //It doesn't grow its size at runtime. To solve this problem, collection framework is used in java.

    //Types of Array
    //1. Single Dimensional Array
    //2. Multi Dimensional Array

    public void singleDimensionalArrayDemo(){
        int[] array=new int[5]; //Declaration and instantiation
        array[0]=10; //Initialization
        array[1]=20;
        array[2]=30;
        array[3]=40;
        array[4]=50;

        for(int i=0;i<array.length;i++){
            System.out.println(array[i]);
        }
    }

    public void multiDimesionalArray(){
        int[][] array=new int[3][3]; //Declaration and instantiation
        array[0][0]=1; //Initialization
        array[0][1]=2;
        array[0][2]=3;
        array[1][0]=4;
        array[1][1]=5;
        array[1][2]=6;
        array[2][0]=7;
        array[2][1]=8;
        array[2][2]=9;

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(array[i][j]+" ");
            }
            System.out.println();
        }
    }

    public void copyArrayToAnotherArray(){
        char[] chars={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p'};
        char[] copyTo= new char[5];

        System.arraycopy(chars,11,copyTo,0,5);
        System.out.println(copyTo);
    }

    public void additionOfTwoMatrices(){
        int[][] array1={
                {1,2,3},
                {4,5,6}
        };

        int[][] array2={
                {3,2,1},
                {6,5,4}
        };

        int[][] array3=new int[2][3];

        for(int i=0;i<2;i++){
            for(int j=0;j<3;j++){
                array3[i][j]=array1[i][j]+array2[i][j];
                System.out.print(array3[i][j]+" ");
            }
            System.out.println();
        }
    }



}
