package com.itbits.AdapterDesignPattern;

/**
 * Created by pravingosavi on 19/09/18.
 */
public class XpayToPayDAdapter implements PayD {

    private String custCardNo;
    private String cardOwnerName;
    private String cardExpMonthDate;
    private Integer cVVNo;
    private Double totalAmount;

    private final XPay xPay;

    public XpayToPayDAdapter(XPay xPay) {
        this.xPay = xPay;
        this.setPro();
    }

    @Override
    public String getCustCardNo() {
        return custCardNo;
    }

    @Override
    public String getCardOwnerName() {
        return cardOwnerName;
    }

    @Override
    public String getCardExpMonthDate() {
        return cardExpMonthDate;
    }

    @Override
    public Integer getCVVNo() {
        return cVVNo;
    }

    @Override
    public Double getTotalAmount() {
        return totalAmount;
    }

    @Override
    public void setCustCardNo(String custCardNo) {
        this.custCardNo = custCardNo;
    }

    @Override
    public void setCardOwnerName(String cardOwnerName) {
        this.cardOwnerName = cardOwnerName;
    }

    @Override
    public void setCardExpMonthDate(String cardExpMonthDate) {
        this.cardExpMonthDate = cardExpMonthDate;
    }

    @Override
    public void setCVVNo(Integer cVVNo) {
        this.cVVNo = cVVNo;
    }

    @Override
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    private void setPro(){
        setCardExpMonthDate(xPay.getCardExpMonth()+xPay.getCardExpYear());
        setCardOwnerName(xPay.getCustomerName());
        setCustCardNo(xPay.getCreditCardNo());
        setCVVNo(xPay.getCardCVVNo().intValue());
        setTotalAmount(xPay.getAmount());

    }
}
