package com.itbits.AdapterDesignPattern;

/**
 * Created by pravingosavi on 19/09/18.
 */
public class RunAdapterExample {

    public static void main(String[] args) {
        XPay xPay =  new XPayImpl();
        xPay.setAmount(3000.50);
        xPay.setCardCVVNo((short) 253);
        xPay.setCardExpMonth("10");
        xPay.setCardExpYear("21");
        xPay.setCreditCardNo("1234567");
        xPay.setCustomerName("Pravin Gosavi");

        PayD payD =  new XpayToPayDAdapter(xPay);
        testExample(payD);
    }

    private static void testExample(PayD payD){
        System.out.println(payD.getCardOwnerName());
        System.out.println(payD.getCustCardNo());
        System.out.println(payD.getCardExpMonthDate());
        System.out.println(payD.getCVVNo());
        System.out.println(payD.getTotalAmount());

    }
}
