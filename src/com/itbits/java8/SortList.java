package com.itbits.java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by pravingosavi on 24/09/18.
 */
public class SortList {

    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<Employee>();
        employeeList.add(new Employee("1","Pravin"));
        employeeList.add(new Employee("2","Sandip"));
        Collections.sort(employeeList, Comparator.comparing(Employee::getId));

    }
}
