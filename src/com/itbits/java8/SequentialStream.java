package com.itbits.java8;

import java.util.List;
import java.util.function.Predicate;

/**
 * Created by pravingosavi on 18/07/18.
 */
public class SequentialStream {



    public static void displayPerson(List<Person> people, Predicate<Person> predicate){
        System.out.println("Selected :");
        people.stream()
                .filter(predicate)
                .forEach(p -> System.out.println(p.getName()));
    }
}



