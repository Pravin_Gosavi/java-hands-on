package com.itbits.JavaBasic;

/**
 * Created by pravingosavi on 13/06/17.
 */
public class PrimeDemo {

    int value1;

    public  void set(int value2){
        this.value1=value2;
    }

    public void decide(){
        int i;
        for(i=2;i<value1;i++){
            if(value1%i==0){
                break;
            }
        }
        if(i==value1){
            System.out.println("PRIME");
        }else{
            System.out.println("NOT PRIME");
        }
    }
}
