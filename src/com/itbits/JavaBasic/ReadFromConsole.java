package com.itbits.JavaBasic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class ReadFromConsole {

    public void read(){
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        int a,b,c;
        try {
            System.out.println("Enter a =");
            a=Integer.parseInt(br.readLine());
            System.out.println("Enter b =");
            b=Integer.parseInt(br.readLine());
            c =a+b;
            System.out.println("Addition of a and b = "+c);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
