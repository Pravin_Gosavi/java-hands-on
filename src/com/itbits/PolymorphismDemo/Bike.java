package com.itbits.PolymorphismDemo;

/**
 * Created by pravingosavi on 20/06/17.
 */

class Vehicle {

    public void run(){
        System.out.println("Vehicle is Running");
    }
}

public class Bike extends Vehicle{

    public void run(){
        System.out.println("Bike is Running Safely");
    }
}
