package com.itbits.PolymorphismDemo;

/**
 * Created by pravingosavi on 19/06/17.
 */
public class MethodOverridingExample {

    //It is also called as Run time Polymorphism

    //If subclass (child class) has the same method as declared in the parent class, it is known as method overriding in java.
    //If subclass provides the specific implementation of the method that has been provided by one of its parent class, it is known as method overriding.

    //Usage of Java Method Overriding
    //Method overriding is used to provide specific implementation of a method that is already provided by its super class.
    //Method overriding is used for runtime polymorphism

    //Rules for Java Method Overriding
    //1. method must have same name as in the parent class
    //2. method must have same parameter as in the parent class.
    //3. must be IS-A relationship (inheritance).




}
