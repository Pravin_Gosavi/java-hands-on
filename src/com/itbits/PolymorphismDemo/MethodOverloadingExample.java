package com.itbits.PolymorphismDemo;

/**
 * Created by pravingosavi on 19/06/17.
 */
public class MethodOverloadingExample {

    //It is also called as Compile time Polymorphism

    //If a class has multiple methods having same name but different in parameters, it is known as Method Overloading.
    //Method overloading increases the readability of the program.

    //There are two ways to overload the method in java
    //1. By changing number of arguments
    //2. By changing the data type

    //Method Overloading: changing no. of arguments

    public int add(int a,int b){
        return a+b;
    }

    public int add(int a,int b,int c){
        return a+b+c;
    }

    public double add(double a,double b){
        return a+b;
    }

    public void sum(int a,long b){
        System.out.println(a+b);
    }

    public void sum(int a,int b,int c){
        System.out.println(a+b+c);
    }

    public void addition(int a,int b){
        System.out.println(a+b);
    }

    public void addition(long a,long b){
        System.out.println(a+b);
    }

    //Method Overloading: changing number of parameters
    public void methodOverloadingByChangingParameters(){
        System.out.println(this.add(11,11));
        System.out.println(this.add(10,10,10));
    }


    //Method Overloading: changing data type of parameters
    public void methodOverloadingByChangingDataTypes(){
        System.out.println(this.add(20,30));
        System.out.println(this.add(10.2,20.1));
    }

    //Method Overloading by Type Promotion
    public void methodOverloadingByTypePromotion(){
        this.sum(10,11);
        this.sum(10,10,20);
    }

    //Method Overloading by Type Promotion when matching found
    public void methodOverloadingByTypePromotionMatchFound(){
        this.addition(10,12);
    }


}
