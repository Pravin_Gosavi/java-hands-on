package com.itbits.WrapperClassesDemo;

/**
 * Created by pravingosavi on 19/06/17.
 */
public class WrapperClassExample {

    //Wrapper class in java provides the mechanism to convert primitive into object and object into primitive.
    //autoboxing - converts primitive into object
    //unboxing - converts object into primitive
    // Primitive Type            Wrapper Type
    // 1.int                        Integer
    // 2.char                       Character
    // 3.boolean                    Boolean
    // 4.short                      Short
    // 5.byte                       Byte
    // 6.long                       Long
    // 7.double                     Double
    // 8.float                      Float


    //autoboxing(primitive to wrapper) example
    public void primitiveToWrapperDemo(){
        int a=10;

        Integer b = Integer.valueOf(a);
        Integer c = a; // autoboxing

        System.out.print(b+" "+c);
    }

    public void WrapperToPrimitiveDemo(){
        Integer a=10;
        int b= a.intValue();
        int c= a; // unboxing
        System.out.println("\n"+b+" "+c);
    }


}
