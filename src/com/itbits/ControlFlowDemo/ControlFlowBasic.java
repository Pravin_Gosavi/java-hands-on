package com.itbits.ControlFlowDemo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pravingosavi on 17/06/17.
 */
public class ControlFlowBasic {

    //Decision-making Statements in Java
    //1. if
    //2. if-else
    //3. switch

    //Looping Statements in java
    //1. for
    //2. while
    //3. do-while

    //Branching Statements in java
    //1. break
    //2. continue
    //3. return


    //The if-then statement is the most basic of all the control flow statements.
    // It tells your program to execute a certain section of code only if a particular test evaluates to true.
    // The if-then-else statement provides a secondary path of execution when an "if" clause evaluates to false.
    public void ifElseDemo(){
        int percentage= 85;
        char grade;

        if(percentage >=90){
            grade='A';
            System.out.println("Grade : "+grade);
        }else if(percentage>=80){
            grade='B';
            System.out.println("Grade : "+grade);
        }else if(percentage>=60){
            grade='C';
            System.out.println("Grade : "+grade);
        }else if(percentage>=40){
            grade='D';
            System.out.println("Grade : "+grade);
        }else{
            System.out.println("Fail..");
        }
    }


    //A switch works with the byte, short, char, and int primitive data types
    //the switch statement allows for any number of possible execution paths.
    public void switchDemo(){
        int month=6;
        String monthString = null;
        switch (month){
            case 1: monthString="January";
                break;
            case 2: monthString="February";
                break;
            case 3: monthString="March";
                break;
            case 4: monthString="April";
                break;
            case 5: monthString="May";
                break;
            case 6: monthString="June";
                break;
            case 7: monthString="July";
                break;
            case 8: monthString="August";
                break;
            case 9: monthString="September";
                break;
            case 10: monthString="October";
                break;
            case 11: monthString="November";
                break;
            case 12: monthString="December";
                break;
            default: monthString="Invalid Month";
                break;
        }
        System.out.println("Month =" +monthString);
    }

    public void getFutureMonthsusingSwitch(){
        int month=8;
        List<String> futureMonths=new ArrayList<>();
        switch (month){
            case 1: futureMonths.add("January");
            case 2: futureMonths.add("February");
            case 3: futureMonths.add("March");
            case 4: futureMonths.add("April");
            case 5: futureMonths.add("May");
            case 6: futureMonths.add("June");
            case 7: futureMonths.add("July");
            case 8: futureMonths.add("August");
            case 9: futureMonths.add("September");
            case 10: futureMonths.add("October");
            case 11: futureMonths.add("November");
            case 12: futureMonths.add("December");
            default: break;
        }

        if(futureMonths.isEmpty()){
            System.out.println("Invalid Month");
        }else{
            for(String monthName: futureMonths){
                System.out.println(monthName);
            }
        }

    }


    //The while and do-while statements continually execute a block of statements while a particular condition is true.
    //The difference between do-while and while is that do-while evaluates its expression at the bottom of the loop instead of the top.
    //Therefore, the statements within the do block are always executed at least once.
    public void whileDemo(){
        int count=1;
        while(count<11){
            System.out.println(count);
            count++;
        }
    }

    public void doWhileDemo(){
        int count=1;
        do{
            System.out.println(count);
            count++;
        }while (count<5);
    }

    public void forLoopDemo(){
        for(int i=0;i<=10;i++){
            System.out.println("Count is : "+i);
        }
    }

    public void enhancedForLoopDemo(){
        int[] numbers={1,2,3,4,5,6,7,8,9,10};

        for(int item:numbers){
            System.out.println("Number count is :"+item);
        }
    }


    //We can use an unlabeled break to terminate a for, while, or do-while loop
    public void breakStatementDemo(){

        int[] numbers={10,12,14,16,18,20};
        int searchFor=20;
        boolean search=false;

        int i;
        for(i=0;i<numbers.length;i++){
            if(numbers[i]==searchFor){
                search=true;
                break;
            }
        }

        if(search){
            System.out.println("Search found at an Index : "+i);
        }else{
            System.out.println("Number not in array");
        }
    }

    public void breakStatementDemo1(){
        int[][] numbers={
                {10,20,30,40},
                {1,2,3,4},
                {3,6,9,12}
        };

        int searchFor = 12;
        boolean flag=false;

        int i,j=0;

        search: for(i=0;i<numbers.length;i++){
                    for(j=0;j<numbers[i].length;j++){
                        if(numbers[i][j]==searchFor){
                            flag=true;
                            break search;
                        }
                    }
        }

        if(flag){
            System.out.println("Found " + searchFor + " at " + i + ", " + j);
        }else{
            System.out.println("Number not in array");
        }
    }


    //The continue statement skips the current iteration of a for, while , or do-while loop
    public void continueStatementDemo(){
        String searchMe="peter piper picked a \" + \"peck of pickled peppers";
        int count=0;

        for(int i=0;i<searchMe.length();i++){
            if(searchMe.charAt(i)!='p'){
                continue;
            }else{
                count++;
            }
        }
        System.out.println("Search Count : "+count);
    }




}


