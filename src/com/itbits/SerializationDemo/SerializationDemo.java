package com.itbits.SerializationDemo;

import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by pravingosavi on 17/06/17.
 */
public class SerializationDemo {

    private String fileName="/Users/pravingosavi/Desktop/employee.txt";

    public void saveEmployeeData(){
        Employee employee=new Employee();
        DataInputStream dataInputStream;
        String name,address,contact;
        try {
            dataInputStream=new DataInputStream(System.in);
            System.out.println("Enter Employee Name : ");
            name=dataInputStream.readLine();
            System.out.println("Enter Employee Address : ");
            address=dataInputStream.readLine();
            System.out.println("Enter Employee Contact : ");
            contact=dataInputStream.readLine();

            employee.setEmployeeName(name);
            employee.setEmployeeAddress(address);
            employee.setEmployeeContact(contact);
            FileOutputStream fileOutputStream=new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(employee);
            System.out.println("Employee Data Written Successfully");
            dataInputStream.close();
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
