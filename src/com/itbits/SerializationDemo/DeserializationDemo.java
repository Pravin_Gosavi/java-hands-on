package com.itbits.SerializationDemo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by pravingosavi on 17/06/17.
 */
public class DeserializationDemo {

    private String fileName="/Users/pravingosavi/Desktop/employee.txt";

    public void readDataFromFile(){
        try {
            Employee employee=new Employee();
            FileInputStream fileInputStream=new FileInputStream(fileName);
            ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
            Object object=objectInputStream.readObject();
            employee=(Employee)object;
            System.out.println("Employee Name : " +employee.getEmployeeName());
            System.out.println("Employee Address : " +employee.getEmployeeAddress());
            System.out.println("Employee Contact : " +employee.getEmployeeContact());
            fileInputStream.close();
            objectInputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
