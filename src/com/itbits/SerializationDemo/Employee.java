package com.itbits.SerializationDemo;

import java.io.Serializable;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class Employee implements  Serializable{

    private String employeeName;
    private String employeeAddress;
    private String employeeContact;

    public Employee() {
    }

    public Employee(String employeeName, String employeeAddress, String employeeContact) {
        this.employeeName = employeeName;
        this.employeeAddress = employeeAddress;
        this.employeeContact = employeeContact;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeAddress() {
        return employeeAddress;
    }

    public void setEmployeeAddress(String employeeAddress) {
        this.employeeAddress = employeeAddress;
    }

    public String getEmployeeContact() {
        return employeeContact;
    }

    public void setEmployeeContact(String employeeContact) {
        this.employeeContact = employeeContact;
    }
}
