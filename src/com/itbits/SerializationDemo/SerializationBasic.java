package com.itbits.SerializationDemo;

/**
 * Created by pravingosavi on 15/06/17.
 */
public class SerializationBasic {

    //Serialization -- is the process of saving the object state permanently in the form of file.
    //This interface does not contain any abstract method and such kind of interfaces are known as marked or tagged interface

    //Deserialization is the process of reducing the data from file in the form Object

    // In java we have four types of serialization; they are complete serialization, selective serialization, manual serialization and automatic serialization.

    //1. Complete serialization is one in which all the data members of the class participates in serialization process.
    //2. Selective serialization is one in which few data members of the class or selected members of the class are participating in serialization process. In order to avoid the variable from the serialization process, make that variable declaration as transient i.e., transient variables never participate in serialization process.
    //3. Manual serialization is one in which the user defined classes always implements java.io.Serializable interface.
    //4. Automatic serialization is one in which object of sub-class of Serializable sub-class participates in serialization process.

}
