package com.itbits.inheritancedemo1;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class DerivedClass1 extends BaseClass1 {

    public void display(){
        //Private Data Member from base class is not accessible from derived class
        //System.out.println("Private Variable from Base Class = "+privateVariable);
        System.out.println("Default Variable from Base Class = "+defaultVariable);
        System.out.println("Protected Variable from Base Class = "+protectedVariable);
        System.out.println("Public Variable from Base Class = "+publicVariable);
    }
}
