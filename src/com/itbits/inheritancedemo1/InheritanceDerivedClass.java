package com.itbits.inheritancedemo1;

import com.itbits.inheritancedemo1.InheritanceBaseClass;

/**
 * Created by pravingosavi on 13/06/17.
 */
public class InheritanceDerivedClass extends InheritanceBaseClass {

    int b;

    public void set(int x,int y){
        super.a=x;
        b=y;
    }

    public void sum(){
        super.display();
        System.out.println("Sum of a and b ="+(a+b));
    }

}
