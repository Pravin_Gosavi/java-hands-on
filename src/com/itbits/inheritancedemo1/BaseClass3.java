package com.itbits.inheritancedemo1;

/**
 * Created by pravingosavi on 14/06/17.
 */

//Has-A relationship and within same package
public class BaseClass3 {

    BaseClass1 baseClass1=new BaseClass1();

    public void display(){
        //System.out.println("Private Variable from Base Class = "+baseClass1.privateVariable);
        System.out.println("Default Variable from Base Class = "+baseClass1.defaultVariable);
        System.out.println("Protected Variable from Base Class = "+baseClass1.protectedVariable);
        System.out.println("Public Variable from Base Class = "+baseClass1.publicVariable);
    }
}
