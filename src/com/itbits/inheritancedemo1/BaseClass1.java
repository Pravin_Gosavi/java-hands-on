package com.itbits.inheritancedemo1;

/**
 * Created by pravingosavi on 14/06/17.
 */
public class BaseClass1 {

    private int privateVariable=10;
    int defaultVariable=20;
    protected int protectedVariable=30;
    public int publicVariable=40;

    public void display(){
        System.out.println("Private Variable from Base Class = "+privateVariable);
        System.out.println("Default Variable from Base Class = "+defaultVariable);
        System.out.println("Protected Variable from Base Class = "+protectedVariable);
        System.out.println("Public Variable from Base Class = "+publicVariable);
    }

}
