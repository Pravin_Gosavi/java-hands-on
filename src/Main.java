import com.itbits.ArrayDemo.ArrayBasic;
import com.itbits.ConstuctorDemo.SuperAtDerivedClass;
import com.itbits.ControlFlowDemo.ControlFlowBasic;
import com.itbits.DataAbstractionDemo.*;
import com.itbits.ExceptionHandling.*;
import com.itbits.FileOperations.DataInputOutputStreamsDemo;
import com.itbits.FileOperations.DataInputStreamReadFile;
import com.itbits.FileOperations.DataOutputStreamWriteFile;
import com.itbits.FileOperations.FileOperationsDemo;
import com.itbits.IntefaceDemo.*;
import com.itbits.JavaBasic.MulificationTable;
import com.itbits.JavaBasic.PrimeDemo;
import com.itbits.PolymorphismDemo.*;
import com.itbits.JavaBasic.ReadFromConsole;
import com.itbits.RecursionInJava.RecursionDemo;
import com.itbits.SerializationDemo.DeserializationDemo;
import com.itbits.WrapperClassesDemo.WrapperClassExample;
import com.itbits.inheritancedemo1.BaseClass1;
import com.itbits.inheritancedemo1.BaseClass3;
import com.itbits.inheritancedemo1.DerivedClass1;
import com.itbits.inheritancedemo2.BaseClass2;
import com.itbits.inheritancedemo2.DerivedClass2;
import com.itbits.inheritancedemo1.InheritanceDerivedClass;
import com.itbits.java8.ParallelStream;
import com.itbits.java8.Person;
import com.itbits.java8.SequentialStream;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws InvalidNumberException {
        System.out.println("Creating a Table for a Given Number");
        MulificationTable mulificationTable=new MulificationTable();
        mulificationTable.set(4);
        mulificationTable.table();

        System.out.println("=========================================================");
        System.out.println("Number is prime or not");

        PrimeDemo primeDemo=new PrimeDemo();
        primeDemo.set(11);
        primeDemo.decide();

        System.out.println("=========================================================");
        System.out.println("Inheritance Demo");
        InheritanceDerivedClass inheritanceDerivedClass=new InheritanceDerivedClass();
        inheritanceDerivedClass.set(20,20);
        inheritanceDerivedClass.sum();

        System.out.println("=========================================================");
        System.out.println("Super keyword at constructor level Demo");
        SuperAtDerivedClass superAtDerivedClass=new SuperAtDerivedClass();

        System.out.println("=========================================================");
        System.out.println("Access Specifier Demo");

        System.out.println("With Respect to Same Package Base Class");
        BaseClass1 baseClass1=new BaseClass1();
        baseClass1.display();

        System.out.println("\n\nWith Respect to Same Package Derived Class");
        DerivedClass1 derivedClass1=new DerivedClass1();
        derivedClass1.display();

        System.out.println("\n\nWith Respect to Same Package Independent Class");
        BaseClass3 baseClass3=new BaseClass3();
        baseClass3.display();

        System.out.println("\n\nWith Respect to Other Package Derived Class");
        BaseClass2 baseClass2=new BaseClass2();
        baseClass2.display();

        System.out.println("\n\nWith Respect to Other Package Independent Class");
        //nameless object approach
        new DerivedClass2().display();

        System.out.println("=========================================================");
        System.out.println("Exception Handling..\n\nTry catch demo\n");
        TryCatchDemo tryCatchDemo=new TryCatchDemo();
        tryCatchDemo.deplayExceptions();

        System.out.println("\n\nThrows Demo");
        new TryCatchBlock().callDivisionMethod();

        System.out.println("\nDemo for finding details of the exception");
        //new FindDetailsOfException().usingAnObjectOfJavaException();
        //new FindDetailsOfException().usingPrintStackTrace();
        //new FindDetailsOfException().usingGetMessage();

        System.out.println("\n\nUser Defined Exceptions Demo");
        UserDefinedExceptionDemo userDefinedExceptionDemo=new UserDefinedExceptionDemo();
        //userDefinedExceptionDemo.checkNumber1();
        userDefinedExceptionDemo.checkNumber2();


        System.out.println("=========================================================");
        System.out.println("Read from Console");
        ReadFromConsole readFromConsole=new ReadFromConsole();
        //readFromConsole.read();

        System.out.println("=========================================================");
        System.out.println("File Operations Demo (FileInputStream/FileOutputStream)\n");
        FileOperationsDemo fileOperationsDemo=new FileOperationsDemo();
        //fileOperationsDemo.writeToFile();
        //fileOperationsDemo.readFromFile();
        //fileOperationsDemo.showFileListFromFolder();
        //fileOperationsDemo.showFilesByExtension();
        //fileOperationsDemo.readFileContentsAsByteArray();
        //fileOperationsDemo.setFilePermissions();

        System.out.println("\n\nFile Operations Demo (DataInputStream/DataOutputStream)\n");
        DataInputOutputStreamsDemo dataInputOutputStreamsDemo=new DataInputOutputStreamsDemo();
        //dataInputOutputStreamsDemo.calculateProduct();
        new DataOutputStreamWriteFile().save();
        new DataInputStreamReadFile().readStudentData();

        System.out.println("=========================================================");
        System.out.println("\n\nSerialization/Deserialization Demo");
        //new SerializationDemo().saveEmployeeData();
        new DeserializationDemo().readDataFromFile();

        System.out.println("=========================================================");
        System.out.println("\n\nControl Flow Statements Demo");
        ControlFlowBasic controlFlowBasic=new ControlFlowBasic();
        controlFlowBasic.ifElseDemo();
        controlFlowBasic.switchDemo();
        controlFlowBasic.getFutureMonthsusingSwitch();
        controlFlowBasic.whileDemo();
        controlFlowBasic.doWhileDemo();
        controlFlowBasic.forLoopDemo();
        controlFlowBasic.enhancedForLoopDemo();
        controlFlowBasic.breakStatementDemo();
        controlFlowBasic.breakStatementDemo1();
        controlFlowBasic.continueStatementDemo();


        System.out.println("=========================================================");
        System.out.println("\n\nArray Demo");
        ArrayBasic arrayBasic=new ArrayBasic();
        arrayBasic.singleDimensionalArrayDemo();
        arrayBasic.multiDimesionalArray();
        arrayBasic.copyArrayToAnotherArray();
        arrayBasic.additionOfTwoMatrices();

        System.out.println("=========================================================");
        System.out.println("\n\nWrapper classes Demo- Autoboxing/Unboxing");
        WrapperClassExample wrapperClassExample=new WrapperClassExample();
        wrapperClassExample.primitiveToWrapperDemo();
        wrapperClassExample.WrapperToPrimitiveDemo();

        System.out.println("=========================================================");
        System.out.println("\n\nMethod Overloading Demo");
        MethodOverloadingExample methodOverloadingExample=new MethodOverloadingExample();
        methodOverloadingExample.methodOverloadingByChangingParameters();
        methodOverloadingExample.methodOverloadingByChangingDataTypes();
        methodOverloadingExample.methodOverloadingByTypePromotion();
        methodOverloadingExample.methodOverloadingByTypePromotionMatchFound();

        System.out.println("=========================================================");
        System.out.println("\n\nMethod Overriding Demo");
        Bike bike=new Bike();
        bike.run();
        System.out.println("ICICI Bank Rate of Interest : "+new ICICI().getRateOfInterest());
        System.out.println("HDFC Bank Rate of Interest : "+new HDFC().getRateOfInterest());
        System.out.println("SBI Bank Rate of Interest : "+new SBI().getRateOfInterest());

        System.out.println("=========================================================");
        System.out.println("\n\nRecursion Demo");
        RecursionDemo recursionDemo=new RecursionDemo();
        recursionDemo.recursionExample();
        recursionDemo.factorialUsingRecursion();
        recursionDemo.fabonacciSeriesUsingRecursion();

        System.out.println("=========================================================");
        System.out.println("Abstract Class Demo");
        AbstractClassDemo abstractClassDemo;
        abstractClassDemo=new IntegerSum();
        abstractClassDemo.sum();
        abstractClassDemo=new FloatSum();
        abstractClassDemo.sum();
        Honda honda=new Honda();
        honda.run();
        Shape shape=new Circle();
        shape.draw();
        Shape shape1=new Rectangle();
        shape1.draw();
        RBI rbi;
        rbi=new IDBI();
        System.out.println("IDBI Rate of Interest : "+rbi.getRateOfInterest());
        rbi=new PNB();
        System.out.println("PNB Rate of Interest : "+rbi.getRateOfInterest());
        AbstractClassWithConstructor abstractClassWithConstructor=new WithConstructorDerivedClass();
        abstractClassWithConstructor.run();
        abstractClassWithConstructor.destroy();

        System.out.println("=========================================================");
        System.out.println("Interfaces Demo");
        DrawRectangle drawRectangle=new DrawRectangle();
        drawRectangle.draw();
        DrawCircle drawCircle=new DrawCircle();
        drawCircle.draw();
        Drawable drawable=new DrawCircle();
        drawable.draw();
        drawable=new DrawRectangle();
        drawable.draw();

        System.out.println("=========================================================");
        System.out.println("Multiple Inheritance Demo");
        MultipleInheritanceBasic multipleInheritanceBasic=new MultipleInheritanceBasic();
        multipleInheritanceBasic.print();
        multipleInheritanceBasic.show();
        TestInterface testInterface=new TestInterface();
        testInterface.print();
        testInterface.show();

        System.out.println("=========================================================");
        System.out.println("Simple Lambda Expressions demo");

        System.out.println("=========================================================");
        System.out.println("Traversing Collection using Sequential streams");
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Pravin",32));
        persons.add(new Person("Sandip",33));
        persons.add(new Person("Hrishi",18));

        Predicate<Person> people = (p) -> p.getAge() > 25;
        new SequentialStream().displayPerson(persons,people);



        System.out.println("=========================================================");
        System.out.println("Traversing Collection using Parallel streams");
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Pravin",32));
        personList.add(new Person("Sandip",33));
        personList.add(new Person("Hrishi",18));

        Predicate<Person> personPredicate = (p) -> p.getAge() > 25;
        new ParallelStream().displayPerson(persons,personPredicate);


        System.out.println("=========================================================");
        System.out.println("Creating Streams from Array");
        Person[] personArray = {
                new Person("Pravin",32),
                new Person("Sandip",33),
                new Person("Hrishi",18)
        };
        Stream<Person> stream = Stream.of(personArray);
        //or  Stream<Person> stream = Arrays.stream(personArray);
        stream.forEach(p -> System.out.println(p.getName()));


    }
}
